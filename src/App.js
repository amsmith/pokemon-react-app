import React from 'react';
import './App.css';
import Pokegame from './pokegame/Pokegame';

function App() {
  return (
    <div className='App'>
      <h1 className='App-title'>Pokegame!</h1>
      <Pokegame />
    </div>
  );
}

export default App;
