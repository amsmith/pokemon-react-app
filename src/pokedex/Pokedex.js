import React, { Component } from 'react';
import Pokecard from '../pokecard/Pokecard';
import './Pokedex.css';

export default class Pokedex extends Component {
  render() {
    let title;
    if (this.props.isWinner) {
      title = <h2 className='Pokedex-winner'>Winning Hand</h2>;
    } else {
      title = <h2 className='Pokedex-loser'>Losing Hand</h2>;
    }

    const { pokemon } = this.props;
    return (
      <div className='Pokedex'>
        {title}
        <h4>Total Experience: {this.props.exp}</h4>
        <div className='Pokedex-cards'>
          {pokemon.map(p => (
            <Pokecard
              id={p.id}
              name={p.name}
              type={p.type}
              exp={p.base_experience}
            />
          ))}
        </div>
      </div>
    );
  }
}
